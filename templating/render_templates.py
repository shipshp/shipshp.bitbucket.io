#! /usr/bin/env python
# -*- coding: utf-8 -*-

try:
    from jinja2 import Environment, FileSystemLoader
except:
    print ("jinja2 templates engine module required")
    
from web_data import texts

STATIC_DIR = '..'
TEMPLATES_DIR = 'templates'
TEMPLATES = ['index.html', 'screenshots.html', 'downloads.html', 'log.html']
    
def create_static(view):
    env = Environment(loader=FileSystemLoader(TEMPLATES_DIR))
    template = env.get_template(view)
    html = template.render(texts)
    with open("%s/%s" % (STATIC_DIR, view), 'w') as static:
        static.write(html)

for view in TEMPLATES:
    create_static(view)

print "Static files generated"
