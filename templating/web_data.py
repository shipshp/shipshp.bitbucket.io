texts = {
    'blog': {
        'title': "Ship's log",
        'posts': {
            'day2': {
                'title': "Second post",
                'data': "July 22, 2014",
                'content': "Blah blah blah",
            },
            'day1': {
                'title': "First post",
                'data': "July 21, 2014",
                'content': "Welcome aboard!",
            }
        }
    },
}

